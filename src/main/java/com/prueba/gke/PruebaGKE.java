package com.prueba.gke;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.websocket.server.PathParam;

@RestController
public class PruebaGKE {

    @GetMapping("/")
    public  String  getSaludo(){
        return "Hola desde Google Kubernetes Engine";
    }
}
